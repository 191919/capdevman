object MainForm: TMainForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Capture Device Manager'
  ClientHeight = 272
  ClientWidth = 383
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblVersion: TLabel
    Left = 7
    Top = 254
    Width = 154
    Height = 13
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Caption = '!!! FOR INTERNAL USE ONLY !!!'
    Color = clRed
    ParentColor = False
  end
  object lstDevices: TListBox
    Left = 7
    Top = 6
    Width = 271
    Height = 236
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = lstDevicesDblClick
  end
  object btnRefresh: TButton
    Left = 289
    Top = 6
    Width = 80
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = '&Refresh'
    TabOrder = 1
    OnClick = btnRefreshClick
  end
  object btnUninstall: TButton
    Left = 289
    Top = 223
    Width = 80
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'R&emove'
    TabOrder = 2
    OnClick = btnUninstallClick
  end
  object rbVideo: TRadioButton
    Left = 289
    Top = 42
    Width = 85
    Height = 13
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Video Device'
    Checked = True
    TabOrder = 3
    TabStop = True
    OnClick = rbVideoClick
  end
  object rbAudio: TRadioButton
    Left = 289
    Top = 59
    Width = 85
    Height = 13
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Audio Device'
    TabOrder = 4
    OnClick = rbAudioClick
  end
  object btnAbout: TButton
    Left = 289
    Top = 246
    Width = 80
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'A&bout'
    TabOrder = 5
    OnClick = btnAboutClick
  end
end
