unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DSUtils, BaseClass, DirectShow9, StdCtrls, ActiveX;

type
  TMainForm = class(TForm)
    lstDevices: TListBox;
    btnRefresh: TButton;
    btnUninstall: TButton;
    rbVideo: TRadioButton;
    rbAudio: TRadioButton;
    lblVersion: TLabel;
    btnAbout: TButton;
    procedure btnRefreshClick(Sender: TObject);
    procedure btnUninstallClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rbVideoClick(Sender: TObject);
    procedure rbAudioClick(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure lstDevicesDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btnAboutClick(Sender: TObject);
begin
  MessageBox(Handle, 'Capture Device Manager 1.0'#13#10#13#10'Copyright (c) 2010, 2011, 2012 jh. All rights reserved.'#13#10#13#10'License Type: UNREGISTERED', 'About', MB_ICONINFORMATION);
end;

procedure TMainForm.btnRefreshClick(Sender: TObject);
var
  E: TSysDevEnum;
  I: Integer;
begin
  lstDevices.Clear;
  E := TSysDevEnum.Create;
  if rbVideo.Checked then
    E.SelectGUIDCategory(CLSID_VideoInputDeviceCategory)
  else
    E.SelectGUIDCategory(CLSID_AudioInputDeviceCategory);
  for I := 0 to E.CountFilters-1 do
  begin
    lstDevices.Items.Add(E.Filters[I].FriendlyName);
  end;
  E.Free;
end;

procedure TMainForm.btnUninstallClick(Sender: TObject);
var
  E: TSysDevEnum;
  S: String;
  I: Integer;
  FM2: IFilterMapper2;
begin
  I := lstDevices.ItemIndex;
  if (I < 0) then
  begin
    MessageBox(Handle, 'No device selected.', 'Error', MB_ICONERROR);
    exit;
  end;
  if (MessageBox(Handle, 'Removal of a capture device is not revokable. If you uninstall a system capture device accidentally, your Windows may be damaged.'#13#10#13#10'Are you really sure to uninstall this device?', 'Confirm', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) <> IDYES) then
  begin
    exit;
  end;
  S := lstDevices.Items[I];
  E := TSysDevEnum.Create;
  if rbVideo.Checked then
    E.SelectGUIDCategory(CLSID_VideoInputDeviceCategory)
  else
    E.SelectGUIDCategory(CLSID_AudioInputDeviceCategory);
  for I := 0 to E.CountFilters-1 do
  begin
    if (S = E.Filters[I].FriendlyName) then
    begin
      if (Succeeded(CocreateInstance(CLSID_FilterMapper2, nil, CLSCTX_INPROC, IID_IFilterMapper2, FM2))) then
      begin
        if rbVideo.Checked then
          FM2.UnregisterFilter(CLSID_VideoInputDeviceCategory, nil, E.Filters[I].CLSID)
        else
          FM2.UnregisterFilter(CLSID_AudioInputDeviceCategory, nil, E.Filters[I].CLSID);
        FM2 := nil;
        break;
      end;
    end;
  end;
  E.Free;

  btnRefreshClick(Sender);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Scaled := FALSE;
  if (GetVersion AND $FF >= 6) then
  begin
    Font.Name := 'MS Shell Dlg';
    Font.Size := 8;
  end
  else
  begin
    Font.Name := 'MS Shell Dlg 2';
    Font.Size := 8;
  end;

  btnRefreshClick(Sender);
  lblVersion.Font.Color := clRed;
  lblVersion.Font.Style := lblVersion.Font.Style + [fsBold];
end;

procedure TMainForm.lstDevicesDblClick(Sender: TObject);
var
  E: TSysDevEnum;
  S: String;
  I: Integer;
  FM2: IFilterMapper2;
  F: IBaseFilter;
begin
  I := lstDevices.ItemIndex;
  if (I < 0) then
  begin
    MessageBox(Handle, 'No device selected.', 'Error', MB_ICONERROR);
    exit;
  end;

  S := lstDevices.Items[I];
  E := TSysDevEnum.Create;
  if rbVideo.Checked then
    E.SelectGUIDCategory(CLSID_VideoInputDeviceCategory)
  else
    E.SelectGUIDCategory(CLSID_AudioInputDeviceCategory);
  for I := 0 to E.CountFilters-1 do
  begin
    if (S = E.Filters[I].FriendlyName) then
    begin
      F := E.GetBaseFilter(I);
      if (FAILED(ShowFilterPropertyPage(Handle, F))) then
      begin
        MessageBox(Handle, 'This filter doesn''t have a property page.', 'Information', MB_ICONINFORMATION);
      end
      else
      begin
        F := nil;
      end;
    end;
  end;
  E.Free;
end;

procedure TMainForm.rbAudioClick(Sender: TObject);
begin
  btnRefreshClick(Sender);
end;

procedure TMainForm.rbVideoClick(Sender: TObject);
begin
  btnRefreshClick(Sender);
end;

initialization
  CoInitialize(nil);

finalization
  CoUninitialize;

end.
